# 各类记录日志log组件学习

**专题目标**：B站学习的各类记录日志的组件，学习了各类记录日志组件的配置方法，包括各类demo。


## 目录
可以通过下面的链接，进入具体版本的教程目录：

- [doc ](./doc) - 学习手册等文档
- [jcl ](./jcl) - 全称为Jakarta Commons Logging，是Apache提供的一个通用日志API
- [jul ](./jul) - JUL全称Java util Logging是java原生的日志框架
- [log4j ](./log4j) - Log4j是Apache下的一款开源的日志框架
- [logback ](./logback) - Logback是由log4j创始人设计的另一个开源日志组件，性能比log4j要好
- [log4j2 ](./log4j2) - Apache Log4j 2是对Log4j的升级版，参考了logback的一些优秀的设计
- [slf4j ](./slf4j) - 简单日志门面(Simple Logging Facade For Java) SLF4


> **slf4j详细使用说明见doc**   
> **上述组件的详细使用说明见doc中的pdf文档**


